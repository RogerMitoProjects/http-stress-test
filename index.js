'use strict';

const fs = require('fs');
// Pesquisa o arquivo javascript de teste com nome definido em process.argv[2]
fs.readdir('./routines/', (err, files) => {
  if (err) console.info('Something was wrong while reading files...');
  const test = files.find(file => file.slice(0, file.length - 3) === process.argv[2]);
  test ? require(`./routines/${test}`) : console.info('Test not found!');
});
