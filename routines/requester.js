'use strict';

let errorCount = 0;
require('dotenv').config();
const async = require('async');
const request = require('request');
const stepDelay = parseInt(process.env.STEP_DELAY);
const requestStep = parseInt(process.env.REQUEST_STEP);
let requestNumber = parseInt(process.env.REQUEST_NUMBER);

function req(url, method, body, contentType, callback) {
  request(
    {
      url,
      method,
      headers: { 'Content-Type': contentType },
      body: body && (contentType === 'application/json' ? JSON.stringify(body) : body)
    },
    (err, res, body) => {
      if (err) {
        callback(err);
      } else if (res.statusCode >= 400) {
        if (res.headers['content-type'] === 'application/json') return callback(body && JSON.parse(body));
        callback(body);
      } else {
        callback(null, body && JSON.parse(body));
      }
    }
  );
}
// Validação dos variáveis de ambiente.
if (Math.sign(requestNumber) === 1 && Math.sign(requestStep) === 1) {
  exports.tester = (url, method, bodyGen, contentType, callback) => {
    // Contabiliza o tempo inicial
    const hrstart = process.hrtime();
    let arraySeries = [];
    let arrayParallel = [];
    // Dispara requestNumber requisições por bloco de forma paralela.
    for (let i = 0; i < requestNumber; i++) {
      arrayParallel.push(callback => {
        req(`${process.env.BASE_PATH}${url}`, method, bodyGen.next().value || null, contentType, (err, res) => {
          // Contator de requisições que falharam
          if (err) errorCount++;
          // Tanto os erros como as respostas são armazenados em um único objeto.
          callback(null, { res, err });
        });
      });
      // Verificação de última iteração
      // Armazena um bloco de requisições seriais em arraySeries. Os blocos serão disparados em um intervalo de stepDelay ms.
      // Restaura o conteúdo de arrayParallel.
      if (arrayParallel.length >= requestStep || i === requestNumber - 1) {
        const arrayTmp = [...arrayParallel];
        arraySeries.push(callback => setTimeout(() => async.parallel(arrayTmp, callback), stepDelay));
        arrayParallel = [];
      }
    }

    async.series(arraySeries, (err, res) => {
      // Contabiliza o tempo final.
      const hrend = process.hrtime(hrstart);

      // Cálculo de tempo de execução.
      console.info(
        `\tExecution time: ${(hrend[0] - Math.ceil(requestNumber / requestStep) * stepDelay * 1e-3) < 0 ? 0 : (hrend[0] - Math.ceil(requestNumber / requestStep) * stepDelay * 1e-3)}s ${hrend[1] / 1e6}ms`
      );
      console.info(`\tError count: ${errorCount}\t\t Failure percentage: ${(errorCount / requestNumber) * 100}%`);
      process.env.SHOW_DATA_ANSWER === 'TRUE' ? console.info(`\tData Answer: ${res}\n`) : null;
      callback(err, res);
    });
  };
} else {
  console.info('Inconsistent parameters...');
}
