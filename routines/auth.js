'use strict';

const uuid = require('uuid/v4');
const { tester } = require('./requester');

(() => {
  switch (process.argv[3]) {
    case 'auth-authentication':
      console.info('Sending credentials...');
      tester(
        '/auth/v1/authentication/token/',
        'POST',
        (function* () {
          while (true) {
            yield { login: 'roger.oliveira@test.com.br', password: 'tfE-aMX-Wsd-k6N' };
          }
        })(),
        'application/json',
        (err, res) => {
          const flatRes = res.flat().map(r => r.res);
          console.info('Getting user info...');
          tester(
            '/auth/v1/authentication/user-info',
            'POST',
            (function* () {
              for (let i = 0, size = flatRes.length; i < size; i++) {
                yield { accessToken: flatRes[i].accessToken };
              }
            })(),
            'application/json',
            (err, res) => {
              console.info('Validating access tokens...');
              tester(
                '/auth/v1/authentication/validate',
                'POST',
                (function* () {
                  for (let i = 0, size = flatRes.length; i < size; i++) {
                    yield { accessToken: flatRes[i].accessToken };
                  }
                })(),
                'application/json',
                (err, res) => {
                  console.info('Test done!');
                }
              );
            }
          );
        }
      );
      break;
    case 'auth-autorisation':
      console.info('Creating rules...');
      tester(
        '/auth/v1/authorisation/role',
        'POST',
        (function* () {
          while (true) {
            yield {
              client: 999999,
              name: uuid().replace(/-/g, ''),
              api: 'financial',
              permissions: { financial: { screen_a: 755 } }
            };
          }
        })(),
        'application/json',
        (err, res) => {
          const flatRes = res.flat().map(r => r.res);
          console.info('Getting rule permissions...');
          tester(
            '/auth/v1/authorisation/role/list',
            'POST',
            (function* () {
              for (let i = 0, size = flatRes.length; i < size; i++) {
                yield {
                  client: 999999,
                  api: 'financial',
                  role: flatRes[i].id
                };
              }
            })(),
            'application/json',
            (err, res) => {
              //console.info(err, res);
              console.info('Test done!');
            }
          );
        }
      );
      break;
    default:
      console.info('Test not found!');
  }
})();
