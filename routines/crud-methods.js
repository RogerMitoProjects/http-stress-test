'use strict';

const { tester } = require('./requester');

(() => {
  switch (process.argv[3]) {
    case 'cities':
      console.info('Sending request...');
      tester(
        '/cities',
        'GET',
        (function* () {
          while (true) {
            yield null;
          }
        })(),
        'application/json',
        (err, res) => {
          console.info('Test done!');
        }
      );
      break;
    default:
      console.info('Test not found!');
  }
})();
