'use strict';

const { tester } = require('./requester');
const body = '<body><h1>Stress Test</h1> <br> <p style="font-color: red;"> {{user}} <br> {{adress}}</p></body>';

(() => {
  switch (process.argv[3]) {
    case 'email-template':
      console.info('Creating templates...');
      tester(
        'email/v1/template',
        'POST',
        (function* () {
          while (true) {
            yield body;
          }
        })(),
        'text/html',
        (err, res) => {
          console.info('Cleaning up templates...');
          const flatRes = res.flat().map(r => r.res);
          tester(
            'email/v1/template',
            'DELETE',
            (function* () {
              for (let i = 0; i < flatRes.length; i++) {
                yield flatRes[i];
              }
            })(),
            'application/json',
            (err, res) => {
              console.info('Test done!');
            }
          );
        }
      );
      break;
    case 'email-send-immediately':
      console.info('Sending emails immediately...');
      tester(
        'email/v1/send',
        'POST',
        (function* () {
          while (true) {
            yield {
              from: 'gitlab@test.com.br',
              to: ['gitlab@test.com.br'],
              cc: [],
              mapping: {
                title: 'Class-D amplifier',
                label: 'Class-D amplifier',
                content: `A class-D amplifier or switching amplifier is an electronic amplifier in which the amplifying devices (transistors, usually MOSFETs) operate as electronic switches, 
                and not as linear gain devices as in other amplifiers. They operate by rapidly switching back and forth between 
                the supply rails, being fed by a modulator using pulse width, pulse density, or related techniques 
                to encode the audio input into a pulse train. The audio escapes through a simple low-pass filter into the loudspeaker. The high-frequency pulses, 
                which can be as high as 6 MHz, are blocked. Since the pairs of output transistors are never conducting at the same 
                time, there is no other path for current flow apart from the low-pass filter/loudspeaker. For this reason, efficiency 
                can exceed 90%.`
              },
              subject: 'Class-D amplifiers',
              template: 'f42d7488-4505-4656-9d3d-a7d597864228',
              attachments: [
                {
                  filename: 'test.txt',
                  content: 'VGVzdCE=',
                  encoding: 'base64',
                  contentType: 'text/plain'
                }
              ]
            };
          }
        })(),
        'application/json',
        (err, res) => {
          console.info('Test done!');
        }
      );
      break;
    case 'email-send-scheduled':
      console.info('Schedulling emails...');
      tester(
        'email/v1/send',
        'POST',
        (function* () {
          while (true) {
            yield {
              from: 'gitlab@test.com.br',
              to: ['gitlab@test.com.br'],
              cc: [],
              mapping: {
                title: 'Class-D amplifier',
                label: 'Class-D amplifier',
                content: `A class-D amplifier or switching amplifier is an electronic amplifier in which the amplifying devices (transistors, usually MOSFETs) operate as electronic switches, 
                and not as linear gain devices as in other amplifiers. They operate by rapidly switching back and forth between 
                the supply rails, being fed by a modulator using pulse width, pulse density, or related techniques 
                to encode the audio input into a pulse train. The audio escapes through a simple low-pass filter into the loudspeaker. The high-frequency pulses, 
                which can be as high as 6 MHz, are blocked. Since the pairs of output transistors are never conducting at the same 
                time, there is no other path for current flow apart from the low-pass filter/loudspeaker. For this reason, efficiency 
                can exceed 90%.`
              },
              subject: 'Scheduled email - Class-D amplifiers',
              template: 'f42d7488-4505-4656-9d3d-a7d597864228',
              attachments: [
                {
                  filename: 'test.txt',
                  content: 'VGVzdCE=',
                  encoding: 'base64',
                  contentType: 'text/plain'
                }
              ],
              sendDate: 72
            };
          }
        })(),
        'application/json',
        (err, res) => {
          console.info('Test done!');
        }
      );
      break;
    default:
      console.info('Test not found!');
  }
})();
