# stress-test

Aplicação utilizada para aferição de performance de microserviços por meio de chamadas http. Essa medição é feita por
meio da contablização dos indicadores de tempo de resposta e taxa percentual de falhas.
O script funciona executando X blocos de chamadas contendo Y requisições cada um.

# Execução

    Comando:  `npm start nome_arquivo_teste nome_teste`

# Cadastro de rotinas

- 1 Crie o arquivo JS que irá armazenar a rotina de testes no diretório "./routines/"

- 2 Envolva todo o conteúdo do arquivo em uma função autoexecutável Notação ES6: `( ( ) => { // Seu teste aqui } )( )`.

  - Obs.: Caso a rotina tenha mais de um teste, é necessário utilizar a estrutura switch-case para especificar o teste a ser executado. Essa informação estará disponívem em `process.argv[3]`.

- 3 Importe o manipulador de testes por meio de:

  `const { tester } = require('./requester');`

- 4 Repasse o conteúdo da requisão para o manipulador `tester` conforme o seguinte:

  `tester(url, method, body, contentType, callback)`

* Metódos públicos:

  - `tester`

    - IN:

      - url: `{String}` - Endpoint do serviço (URI).
      - method: `{String}` - Metódo http utilizado na requisção ("POST", "GET", "DELETE", etc.).
      - body: `{Object} or {String}` - Conteúdo do corpo da requisição. Esse parâmetro poderá ser uma string caso esteja utilizando uma codificação diferente de "application/json". É importante destacar que, o conteúdo de "body" é o mesmo para cada requisição, uma vez que o seu conteúdo é replicado por meio de uma função geradora (generator function).
      - contentType: `{String}` - Especifica a estrutura do corpo da requisição. Ex.: application/json, text/html ou text/plain.
      - callback: `Function` - Função de retorno contendo os parâmetros de resposta e erro do teste. `(err, res) => { }`. Caso haja alguma falha relacionada à requisição, um erro 500 por exemplo, elas serão anexadas ao objeto de resposta `res` que terá a seguinte estrutura: `{err, res}`. Dessa forma os acessos as respostas ou aos erros poderão ser realizados da seguinte forma: `res.res.forEach(el => console.log(el)) ou res.err.forEach(el => console.log(el))`.

    - OUT:
      - res: Resposta da requisição em caso de sucesso.
      - err: Resposta da requisição em caso de falha.

* Variáveis de ambiente:
  - BASE_PATH `{String}` - Host onde está hospedado o serviço.
  - REQUEST_NUMBER `{Integer}` - Número total de requisições a serem executadas pelo teste.
  - REQUEST_STEP `{Integer}` - Quantidade de requisões por bloco.
  - STEP_DELAY `{Integer}` - Tempo de espera em ms entre os blocos.
  - SHOW_DATA_ANSWER `{Boolean}` - Exibe ou omite a resposta (se houver) de cada requisição. 'TRUE' a respostaé exibida no terminal, 'FALSE' a resposta é omitida.

# Exemplo de utilização

![Getting Started](./stress-test01.jpg)

```javascript
// test.js
'use strict';

const { tester } = require('./requester');

(() => {
  switch (process.argv[3]) {
    case 'test-one':
      console.info('Sending requests...');
      tester(
        'email/v1/template',
        'POST',
        (function*() {
          while (true) {
            yield {
              from: 'gitlab@test.com.br',
              to: ['gitlab@test.com.br'],
              subject: 'Class-D amplifiers',
              template: 'f42d7488-4505-4656-9d3d-a7d597864228',
              attachments: [
                {
                  filename: 'test.txt',
                  content: 'VGVzdCE=',
                  encoding: 'base64',
                  contentType: 'text/plain'
                }
              ]
            };
          }
        })(),
        'text/html',
        (err, res) => {
          console.info('Test done!');
        }
      );
      break;
    case 'test-two':
      console.info('Another test...');
      break;
    default:
      console.info('Test not found!');
  }
})();
```
